$(document).ready(function() {
	asignarEventos();
	loadTaulaOK();
});


function asignarEventos(){
	$('#dataIniciAlta').datepicker({
			showOn: 'button',
		  defaultDate: "+1w",
		  todayHighlight: true,
		  autoclose: true,
      changeMonth: true,
      changeYear: true,
      maxDate: 0, //fecha de hoy
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      weekStart: 1,
     language: 'ca',
      onClose: function( selectedDate ) {
        $( "#dataIniciAlta" ).datepicker( "option", "minDate", selectedDate );
      }
	});
	$('#dataFinalAlta').datepicker({
			showOn: 'button',
		  defaultDate: "+1w",
		  todayHighlight: true,
		  autoclose: true,
      changeMonth: true,
      changeYear: true,
      maxDate: 0, //fecha de hoy
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      weekStart: 1,
     language: 'ca',
      onClose: function( selectedDate ) {
        $( "#dataIniciAlta" ).datepicker( "option", "minDate", selectedDate );
      }
	});
}

