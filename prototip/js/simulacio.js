$(document).ready(function() 
	{
		asignarEventos();
		loadTaulaOK();
	}
);

function asignarEventos(){
	$('#dataIniciAlta').datepicker({
			showOn: 'button',
		  defaultDate: "+1w",
		  todayHighlight: true,
		  autoclose: true,
      changeMonth: true,
      changeYear: true,
      maxDate: 0, //fecha de hoy
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      weekStart: 1,
     language: 'ca',
      onClose: function( selectedDate ) {
        $( "#dataIniciCalcul" ).datepicker( "option", "minDate", selectedDate );
      }
	});
	$('#dataFinalAlta').datepicker({
			showOn: 'button',
		  defaultDate: "+1w",
		  todayHighlight: true,
		  autoclose: true,
      changeMonth: true,
      changeYear: true,
      maxDate: 0, //fecha de hoy
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      weekStart: 1,
     language: 'ca',
      onClose: function( selectedDate ) {
        $( "#dataIniciCalcul" ).datepicker( "option", "minDate", selectedDate );
      }
	});
}




function loadTaulaOK() {
	$("#taulaNouReduccio").tablesorter({
		headers: {
		  2:  {sorter: false},
		  16: {sorter: false},
		  19: {sorter: false}
		}
	});  
}

